﻿using System.Collections.Generic;

class Program
{
    static int[,] adjacencyMatrix = {
        { 0,135,-1,-1,-1,-1,-1,-1,-1,-1, 78,-1,-1,-1,-1,-1,128,-1,-1}, // Київ
        {135,  0, 80,-1,-1, 38,-1,-1,-1,115,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Житомир
        { -1, 80,  0,100,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Новоград-Волинський
        { -1, -1,100,  0, 68,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Рівне
        { -1, -1, -1, 68,  0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Луцьк
        { -1, 38, -1,-1,-1,  0, 73,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Бердичів
        { -1, -1, -1,-1,-1, 73,  0,110,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Вінниця
        { -1, -1, -1,-1,-1,-1,110,  0,104,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Хмельницький
        { -1, -1, -1,-1,-1,-1,-1,104,  0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Тернопіль
        { -1,115, -1,-1,-1,-1,-1,-1,-1,  0,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Шепетівка
        { 78, -1, -1,-1,-1,-1,-1,-1,-1,-1,  0,115,146,-1,181,-1,-1,-1,-1}, // Біла Церква
        { -1, -1, -1,-1,-1,-1,-1,-1,-1,-1,115,  0,-1,-1,-1,-1,-1,-1,-1}, // Умань
        { -1, -1, -1,-1,-1,-1,-1,-1,-1,-1,146,-1,  0,105,-1,-1,-1,-1,-1}, // Черкаси
        { -1, -1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,105,  0,-1,-1,-1,-1,-1}, // Кременчук
        { -1, -1, -1,-1,-1,-1,-1,-1,-1,-1,181,-1,-1,-1,  0,130,-1,-1,-1}, // Полтава
        { -1, -1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,130,  0,-1,-1,-1}, // Харків
        {128, -1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  0,175,109}, // Прилуки
        { -1, -1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,175,  0,-1},  // Суми
        { -1, -1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,109, -1,  0}  // Миргород
    };

    static bool[] visited;
    static List<int> path;
    static int[] distances;

    static void Main(string[] args)
    {
        int startNode = 0; // Київ

        Console.WriteLine("Depth-First Search (DFS):");
        visited = new bool[adjacencyMatrix.GetLength(0)];
        path = new List<int>();
        DFS(startNode, 0);

        Console.WriteLine("\nBreadth-First Search (BFS):");
        visited = new bool[adjacencyMatrix.GetLength(0)];
        distances = new int[adjacencyMatrix.GetLength(0)];
        BFS(startNode);
    }

    static void DFS(int node, int distance)
    {
        visited[node] = true;
        path.Add(node);
        Console.WriteLine($"{string.Join(" -> ", path)} (відстань: {distance})");

        for (int i = 0; i < adjacencyMatrix.GetLength(1); i++)
        {
            if (adjacencyMatrix[node, i] != -1 && !visited[i])
            {
                DFS(i, distance + adjacencyMatrix[node, i]);
            }
        }

        path.RemoveAt(path.Count - 1);
    }

    static void BFS(int startNode)
    {
        Queue<int> queue = new Queue<int>();
        queue.Enqueue(startNode);
        visited[startNode] = true;
        distances[startNode] = 0;

        while (queue.Count > 0)
        {
            int node = queue.Dequeue();
            Console.WriteLine($"{node} (відстань: {distances[node]})");

            for (int i = 0; i < adjacencyMatrix.GetLength(1); i++)
            {
                if (adjacencyMatrix[node, i] != -1 && !visited[i])
                {
                    queue.Enqueue(i);
                    visited[i] = true;
                    distances[i] = distances[node] + adjacencyMatrix[node, i];
                }
            }
        }
    }
}
